package cn.wcg.crawler;

import java.io.IOException;
import java.util.*;

import static cn.wcg.crawler.HBaseApi.*;

public class DataAnalysis {
    public static void main(String[] args) {
        init();
        analysis_education_income();
        close();
    }

    public static void analysis_education_income(){//1
        try {
            List<Integer> min_wages = get_column_list("data", "work", "min_wage", "Integer");
            List<Integer> max_wages = get_column_list("data", "work", "max_wage", "Integer");
            List<String> educations = get_column_list("data", "work", "education", "String");
            String[] edu = {"初中","中专","高中","大专","本科","硕士","博士"};
            for (int i = 0; i < edu.length; i++) {
                int count = 0;
                int shu = 0;
                for (int j = 0; j < min_wages.size(); j++) {
                    if (educations.get(j).equals(edu[i])){
                        count += (min_wages.get(j)+max_wages.get(j))/2;
                        shu++;
                    }
                }
                System.out.println("\""+edu[i]+ "\"" + ":" + count/shu+",");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_income_level(){//1
        try {
            List<Integer> min_wages = get_column_list("data", "work", "min_wage", "Integer");
            List<Integer> max_wages = get_column_list("data", "work", "max_wage", "Integer");
            int l1 = 0;
            int l2 = 0;
            int l3 = 0;
            for (int i = 0; i < min_wages.size(); i++) {
                if (max_wages.get(i)<5000){
                    l1++;
                }else if (min_wages.get(i)>=5000&&max_wages.get(i)<10000){
                    l2++;
                }else if (max_wages.get(i)>=10000){
                    l3++;
                }
            }
            System.out.println(l1);
            System.out.println(l2);
            System.out.println(l3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_most_need_company(){//1
        try {
            List<String> company = get_column_list("data", "company", "company", "String");
            List<Integer> people = get_column_list("data", "work", "nums", "Integer");
            assert company != null;
            HashSet<String> set = new HashSet<>(company);
            HashMap<String, Integer> map = new HashMap<>();
            for (String s : set) {
                int count = 0;
                for (int i = 0; i < company.size(); i++) {
                    if (company.get(i).equals(s)){
                        count+=people.get(i);
                    }
                }
                map.put(s, count);
            }
            //利用Map的entrySet方法，转化为list进行排序
            List<Map.Entry<String, Integer>> entryList = new ArrayList<>(map.entrySet());
            //利用Collections的sort方法对list排序
            Collections.sort(entryList, new Comparator<Map.Entry<String, Integer>>() {
                @Override
                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                    //正序排列，倒序反过来
                    return o2.getValue() - o1.getValue();
                }
            });
            //遍历排序好的list，一定要放进LinkedHashMap，因为只有LinkedHashMap是根据插入顺序进行存储
            LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<String, Integer>();
            for (Map.Entry<String,Integer> e : entryList
            ) {
                linkedHashMap.put(e.getKey(),e.getValue());
                System.out.println("\""+e.getKey()+ "\"" + ":" + e.getValue()+",");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_nature_income(){//1
        String[] nature = {"股份制企业", "上市公司","事业单位","外商独资","合资","其它","民营","国企"};
        int[] num = {207, 10,18,38,58,243,4296,95};
        try {
            List<Integer> min_wages = get_column_list("data", "work", "min_wage", "Integer");
            List<Integer> max_wages = get_column_list("data", "work", "max_wage", "Integer");
            List<String> c_n = get_column_list("data", "company", "nature", "String");
            for (int s = 0; s < nature.length; s++) {
                int count = 0;
                for (int i = 0; i < min_wages.size(); i++) {
                    if (c_n.get(i).equals(nature[s])){
                        count+=(min_wages.get(i)+max_wages.get(i))/2;
                    }
                }
                System.out.println(count/num[s]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_avg_income(){//1
        try {
            List<Integer> min_wages = get_column_list("data", "work", "min_wage", "Integer");
            List<Integer> max_wages = get_column_list("data", "work", "max_wage", "Integer");
            int count = 0;
            for (int i = 0; i < min_wages.size(); i++) {
                if (min_wages.get(i)!= -1){
                    count+=(min_wages.get(i)+max_wages.get(i))/2;
                }
            }
            System.out.println(count/5409);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_company_num(){//1
        try {
            List<String> company = get_column_list("data", "company", "company", "String");
            assert company != null;
            HashSet<String> set = new HashSet<>(company);
            System.out.print(set.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void  print_integer(Map<String, Integer> map){
        System.out.println("{");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println("\""+entry.getKey()+ "\"" + ":" + entry.getValue()+",");
        }
        System.out.println("}");
    }

    public static void print_string(Map<String, String> map){
        System.out.println("{");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println("\""+entry.getKey()+ "\"" + ":\"" + entry.getValue()+"\",");
        }
        System.out.println("}");
    }

    public static void analysis_education(){
        try {
            List<String> educations = get_column_list("data", "work", "education", "String");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < educations.size(); i++) {
                String key = educations.get(i);
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_industry(){
        try {
            List<String> ages = get_column_list("data", "company", "industry", "String");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < ages.size(); i++) {
                String key = ages.get(i);
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_nature(){
        try {
            List<String> ages = get_column_list("data", "company", "nature", "String");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < ages.size(); i++) {
                String key = ages.get(i);
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_people(){
        try {
            List<Integer> ages = get_column_list("data", "work", "nums", "Integer");
            Integer counter = 0;
            for (int i = 0; i < ages.size(); i++) {
                counter +=ages.get(i);
            }
            System.out.println(counter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_work(){
        try {
            List<String> ages = get_column_list("data", "work", "work", "String");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < ages.size(); i++) {
                String key = ages.get(i);
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_wage() {
        try {
            List<Integer> min_wages = get_column_list("data", "work", "min_wage", "Integer");
            List<Integer> max_wages = get_column_list("data", "work", "max_wage", "Integer");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < min_wages.size(); i++) {
                String key = String.valueOf(min_wages.get(i)) + "-" + String.valueOf(max_wages.get(i));
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_experience() {
        try {
            List<Integer> min_experiences = get_column_list("data", "work", "min_experience", "Integer");
            List<Integer> max_experiences = get_column_list("data", "work", "max_experience", "Integer");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < min_experiences.size(); i++) {
                String key = String.valueOf(min_experiences.get(i)) + "-" + String.valueOf(max_experiences.get(i));
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_age() {
        try {
            List<String> ages = get_column_list("data", "work", "age", "String");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < ages.size(); i++) {
                String key = ages.get(i);
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void analysis_address() {
        try {
            List<String> address = get_column_list("data", "work", "address", "String");
            HashMap<String, Integer> map = new HashMap<>();
            for (int i = 0; i < address.size(); i++) {
                String key = address.get(i);
                Integer b = key.lastIndexOf("（");
                Integer e = key.lastIndexOf("）");
                key = key.substring(b + 1, e);
                if (map.get(key) == null) {
                    map.put(key, 1);
                } else {
                    Integer temp = map.get(key);
                    map.put(key, ++temp);
                }
            }
            print_integer(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
