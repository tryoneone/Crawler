package cn.wcg.crawler;


import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;

public class Crawler {
    public static void main(String[] args) throws URISyntaxException {

        //1.打开浏览器 创建一个httpClient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //2.输入网址, 发起get请求创建HttpGet对象
        // 设置请求地址
        // 创建URIBuilder
        URIBuilder uriBuilder = new URIBuilder("http://www.ynhr.com/index.php?m=&c=jobs&a=jobs_list&page=1");
        // 设置参数
//        uriBuilder.setParameter("key","红塔");
        // 创建HttpGet对象，设置url访问地址
        HttpGet httpGet = new HttpGet(uriBuilder.build());
        // 配置请求信息
        RequestConfig config = RequestConfig.custom().setConnectTimeout(1000)//创建连接的最长时间，单位毫秒
                .setConnectionRequestTimeout(500) //设置获取连接的最长时间。单位毫秒
                .setSocketTimeout(10*1000) // 设置数据传输的最长时间，单位毫秒
                .build();
        // 给请求设置请求信息
        httpGet.setConfig(config);
        CloseableHttpResponse response = null;
        try {
            //3.使用HttpClient发起请求 返回响应
            response = httpClient.execute(httpGet);
            //4.解析响应 ，获取数据
            //判断状态码是否是200-响应没有问题
            if(response.getStatusLine().getStatusCode() == 200){
                HttpEntity httpEntity = response.getEntity();
                String content = EntityUtils.toString(httpEntity, "utf8");
                System.out.println(content);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }finally {
            //关闭response
            try {
                response.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            //关闭httpClient
            try {
                httpClient.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }





    }
}
