从功能上讲，爬虫一般分为数据采集、处理、存储三个部分。爬虫从一个或若干初始
网页的URL开始，获得初始网页上的URL，在抓取网页的过程中，不断从当前页面上抽取新的URL放入队列，直到满足系统
的一定停止条件
HttpClient Java的HTTP协议客户端

**连接池**

如果每次请求都要创建HttpClient，会有频繁创建和销毁的问题，可以使用连接池来解决这个问题

**请求参数**

有时因为网络或者目标服务器的原因，请求需要更长的时间才能完成，我们需要自定义相关时间

**Jsoup**

我们抓取到页面之后，还需要对页面进行解析。可以使用字符串处理工具解析页面，也可以使用正则表达式，但是这些方法都会带来
很大的开发成本。

jsoup是一款java的HTML解析器，可直接解析某个URL地址、HTML文本内容。提供了一套非常省力的API，
可以通过DOM，CSS以及类似于JQuery的操作方法来取出和操作数据

Jsoup的主要功能如下：

1.从一个URL、文件或字符串中解析HTML

2.使用DOM或者CSS选择器来查找、取出数据

3.可操作HTML元素、属性、文本。

虽然Jsoup可以替代HttpClient直接发起请求解析数据，但是往往不会这样使用。因为在实际的开发
过程中，需要使用多线程，连接池，代理等等方式，而jsoup对这些的支持并不是很好，所以我们一般把jsoup仅仅作为
HTML解析工具使用


**使用dom方式遍历文档**
元素获取
根据id查询元素getElementById
根据标签获取元素getElementByTag
根据class获取元素getElementByClass
根据属性获取元素getElementByAttribute


//    使用选择器Selector 代码量更少

//    tagname:通过标签查找元素,eg:span

//    #id:通过ID查找元素

//    .class:通过class名称查找元素

//    [attribute]: 利用属性查找元素

//    [attr=value]: 利用属性值来查找元素 class=s_name

**选择器组合**

el#id 元素+id（h3#id)

el[attr]:元素+属性名 span[abc]

el.class:元素+class li.class_a

任意组合：span[abc].s_name

ancestor child :查找某个元素下的自元素 比如：.city_con li - 查找city_con下的所有li

parent > child 查找某个父元素下的直接子元素 ： .city_con > ul > li

parent > * 查找某个父元素下所有直接子元素